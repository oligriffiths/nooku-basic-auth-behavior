<?php
/**
 * Nooku Framework - http://www.nooku.org
 *
 * @copyright	Copyright (C) 2011 - 2013 Timble CVBA and Contributors. (http://www.timble.net)
 * @license		GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link		git://git.assembla.com/nooku-framework.git
 */

namespace Nooku\Component\Application;

use Nooku\Library;

/**
 * Application dispatcher behavior authenticatable
 *
 * @author  Oli Griffiths <oli@timble.net>
 * @package Nooku\Component\Application
 */
class DispatcherBehaviorAuthenticatorBasic extends DispatcherBehaviorAuthenticatorAbstract
{
    /**
     * Allow login via HTTP basic auth
     *
     * @param Library\CommandContext $context
     */
    protected function _actionAuthenticate(Library\CommandContext $context)
    {
        if($context->request->headers->get('php-auth-user') && !$context->user->getId())
        {
            //Create a new request to not modify the original
            $user_context = $this->getCommandContext();
            $user_context->request = clone $context->request;
            $user_context->request->data->set('email', $user_context->request->headers->get('php-auth-user'));
            $user_context->request->data->set('password', $user_context->request->headers->get('php-auth-pw'));
            $user_context->request->headers->set('Referer', (string) $context->request->getUrl());

            //Log the user in
            $this->getObject('com:users.controller.session')->authenticate($user_context);
            $context->http_auth = true;
        }
    }
}