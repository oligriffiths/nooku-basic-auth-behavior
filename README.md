#Nooku Basic Auth Behavior

This is a prototype of a basic auth behavior for nooku 13.1. It is very alpha, but it works, however there may be a better way to do this, I just had to get it working.

To install, add to /component/application/dispatcher/behavior/authenticator

And add the following line to /application/admin/component/application/bootstrapper.php and /application/site/component/application/bootstrapper.php in the bootstrap() function.

	Library\ObjectManager::setConfig('com:application.dispatcher', array('behaviors' => array('com:application.dispatcher.behavior.authenticator.basic')));

