<?php
/**
 * Nooku Framework - http://www.nooku.org
 *
 * @copyright	Copyright (C) 2011 - 2013 Timble CVBA and Contributors. (http://www.timble.net)
 * @license		GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link		git://git.assembla.com/nooku-framework.git
 */

namespace Nooku\Component\Application;

use Nooku\Library;

/**
 * Application dispatcher behavior authenticatable
 *
 * @author  Oli Griffiths <oli@timble.net>
 * @package Nooku\Component\Application
 */
abstract class DispatcherBehaviorAuthenticatorAbstract extends Library\ControllerBehaviorAbstract
{
    /**
     * Object constructor
     *
     * @param Library\Config $config
     */
    public function __construct(Library\ObjectConfig $config)
    {
        parent::__construct($config);

        $this->registerCallback('before.route', array($this, 'authenticate'));
        $this->registerCallback('after.authenticate', array($this, 'settoken'));
    }


    /**
     * @param Library\CommandContext $context
     */
    abstract protected function _actionAuthenticate(Library\CommandContext $context);


    /**
     * After authenticating, fake referrer and token
     *
     * @param Library\CommandContext $context
     */
    protected function _actionSettoken(Library\CommandContext $context)
    {
        if($context->http_auth && $context->user->getId()){
            $url = clone $context->request->getUrl();
            $url->user = null;
            $url->pass = null;

            $context->request->headers->set('Referer', (string) $url); //@TODO: Probably shouldn't be doing this, but stuff breaks if it's not set
            $context->request->headers->set('X-Token', $context->user->session->getToken());
            $context->request->cookies->set('_token', $context->user->session->getToken());
        }
    }
}